
describe('API testing', () => {
    
  Cypress.config('baseUrl','https://jsonplaceholder.typicode.com')
  const item ={"title":"this is a test","body":"just a test","userId":"2"}
  
      
  it('POST - create new record', () => {
    cy.request('POST','/posts', item)
    .its('body')
    .should('include', {body:'just a test'})
  })

  it('PUT - update a record', () => {
    cy.request('PUT','/posts/1', item)
    .its('body')
    .should('include', {body:'just a test'})
  })
  it('GET - get all posts', () => {
    cy.request('GET','/posts').then((response) => {
      expect(response).to.have.property('status', 200)
      expect(response.body).not.to.be.null
    })    
  })

  it('GET - show existing post', () => {
    cy.request('GET','/posts/1').then((response) => {
      expect(response).to.have.property('status', 200)
      expect(response.body.body).to.include('strum rerum est autem sunt rem eveniet architecto')
    })    
  })

  it('GET - filter', () => {
    cy.request('GET','/comments?postId=1').then((response) => {
      expect(response).to.have.property('status', 200)
      expect(response.body.body).not.to.be.null
    })    
  })

  it('DELETE - delete a record', () => {
    cy.request('DELETE','/posts/1').then((response) => {
    expect(response).to.have.property('status', 200)
    expect(response.body).to.be.empty
  })
})

  
  

  })



